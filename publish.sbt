crossPaths := false

publishTo <<= version { (v: String) =>
  if (v.trim.endsWith("-SNAPSHOT"))
    Some(Resolver.file("Snapshots", file("../davidharcombe.github.io/snapshots/")))
  else
    Some(Resolver.file("Releases", file("../davidharcombe.github.io/releases/")))
}