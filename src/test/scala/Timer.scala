object Timer {
  def apply[T](f: ⇒ T): T = {
    val s = System.currentTimeMillis
    val r = f
    println(s"Complete in ${System.currentTimeMillis - s}ms")
    r
  }

  def apply[T](o: String, f: ⇒ T): T = {
    val s = System.currentTimeMillis
    val r = f
    println(s"$o: ${System.currentTimeMillis - s}ms")
    r
  }
}