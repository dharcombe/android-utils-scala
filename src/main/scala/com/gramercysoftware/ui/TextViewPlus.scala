package com.gramercysoftware.ui

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Typeface
import android.util.AttributeSet
import android.widget.TextView

import com.gramercysoftware.R

class TextViewPlus(val context: Context, val attrs: AttributeSet, val defStyle: Int) extends TextView(context, attrs, defStyle) {
  def this(context: Context) = this(context, null, 0)
  def this(context: Context, attrs: AttributeSet) = this(context, attrs, 0)

  val TAG: String = "TextView"

  setCustomFont(context, attrs)

  def setCustomFont(context: Context, typefaceName: String): Boolean = {
    try {
      var tf = Typeface.createFromAsset(context.getAssets, typefaceName)
      setTypeface(tf)
      true
    } catch {
      case ex: Exception ⇒ false
    }
  }

  private def setCustomFont(context: Context, attrs: AttributeSet) {
    var a: TypedArray = context.obtainStyledAttributes(attrs, R.styleable.TextViewPlus)
    var customFontName = a.getString(R.styleable.TextViewPlus_customFont)
    setCustomFont(context, customFontName)
  }
}