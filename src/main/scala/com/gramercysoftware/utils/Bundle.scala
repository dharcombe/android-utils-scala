package com.gramercysoftware.utils

import android.content.Intent
import android.os.{ Bundle ⇒ AndroidBundle }
import android.os.{ Bundle ⇒ AndroidBundle }
import utils._

class BoxedBundle(val rawBundle: AndroidBundle) {
  val bundle = rawBundle match {
    case null ⇒ None
    case b ⇒ Some(b)
  }

  def exists(key: String) =
    (optionallyGet { rawBundle.get(key) }) isDefined

  def get[T](key: String)(implicit m: Manifest[T]): T = {
    val result = m.runtimeClass.getSimpleName match {
      case "String" ⇒ rawBundle.getString(key)
      case "int" ⇒ rawBundle.getInt(key)
      case "double" ⇒ rawBundle.getDouble(key)
      case "long" ⇒ rawBundle.getLong(key)
      case "boolean" ⇒ rawBundle.getBoolean(key)
      case _ ⇒ rawBundle.get(key)
    }
    result.asInstanceOf[T]
  }

  def getOption[T](key: String)(implicit m: Manifest[T]): Option[T] = optionallyGet(get[T](key))

  def put[T](key: String, value: T)(implicit m: Manifest[T]) = {
    val result = m.runtimeClass.getSimpleName match {
      case "String" ⇒ rawBundle.putString(key, value.asInstanceOf[String])
      case "int" ⇒ rawBundle.putInt(key, value.asInstanceOf[Int])
      case "double" ⇒ rawBundle.putDouble(key, value.asInstanceOf[Double])
      case "long" ⇒ rawBundle.putLong(key, value.asInstanceOf[Long])
      case "boolean" ⇒ rawBundle.putBoolean(key, value.asInstanceOf[Boolean])
      case _ ⇒ rawBundle.putSerializable(key, value.asInstanceOf[Serializable])
    }
    this
  }
}

object BundleConversions {
  implicit def bundle2BoxedBundle(bundle: AndroidBundle) = new BoxedBundle(bundle)
  implicit def boxedBundle2bundle(bundle: BoxedBundle) = bundle.rawBundle
}

object Bundle {
  def apply(intent: Intent) = new BoxedBundle(intent.getExtras)
  def apply(bundle: AndroidBundle) = new BoxedBundle(bundle)
}