package com.gramercysoftware.utils

import android.util.Log

trait Retrier[T] {
  def retry(f: ⇒ T, init: Long = 2000, maxRetries: Int = 10, count: Int = 0): T = {
    try {
      f
    } catch {
      case e: Throwable if count < maxRetries ⇒
        Log.e("Retrier", s"Error ${e.getStackTrace().mkString("\n")}: attempt ${count + 1}")
        Thread.sleep(init)
        retry(f, init * 2, maxRetries, count + 1)
      case e: Throwable ⇒
        Log.e("Retrier", s"Error $e: no more attempts")
        throw e
    }
  }
}
