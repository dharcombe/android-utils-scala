package com.gramercysoftware.utils;

import android.content.Context;
import android.util.Log

/*
 * SystemServiceUtils
 * Copyright (C) 2011 David Harcombe
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
object SystemServiceUtils {
  def systemServiceObject[T <: Context](context: Context, service: String): T = {
    context.getSystemService(service) match {
      case null ⇒
        Log.e("SystemServiceUtils", s"Service ${service} not found")
        throw new Exception(s"Service ${service} not found")
      case s: T ⇒ s
      case x ⇒
        Log.e("SystemServiceUtils", s"Requested ${service} of type ${x}")
        throw new ClassCastException(s"Requested ${service} of type ${x}")
    }
  }
}
