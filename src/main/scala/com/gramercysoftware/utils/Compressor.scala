package com.gramercysoftware.utils

import java.util.zip.{ GZIPOutputStream, GZIPInputStream }
import java.io.{ ByteArrayOutputStream, ByteArrayInputStream }
import android.util.Log
import java.io.InputStream

object Compressor {
  def gzip(contents: Array[Byte]): Array[Byte] = {
    val outputStream = new ByteArrayOutputStream
    try {
      val gzipOutputStream = new GZIPOutputStream(outputStream)
      gzipOutputStream.write(contents)
      gzipOutputStream.close
      outputStream.toByteArray
    } catch {
      case e: Exception ⇒
        Log.e("Compressor", "Error compressing payloads due to %s".format(e.getMessage))
        Log.d("Compressor", "%s: %s".format(e.getMessage, e.getStackTrace.map(_.toString).mkString("\n")))
        throw e
    }
  }

  def unGzip(contents: Array[Byte]): Array[Byte] = unGzip(new ByteArrayInputStream(contents))

  def unGzip(stream: InputStream): Array[Byte] = {
    try {
      val gis = new GZIPInputStream(stream)
      Iterator continually gis.read() takeWhile (-1 !=) map (_.toByte) toArray
    } catch {
      case e: Exception ⇒
        Log.e("Compressor", "Error decompressing payloads due to %s".format(e.getMessage))
        Log.d("Compressor", "%s: %s".format(e.getMessage, e.getStackTrace.map(_.toString).mkString("\n")))
        throw e
    }
  }

  def isGzip(contents: Array[Byte]) = {
    if ((contents == null) || (contents.length < 2)) false
    else ((contents(0) == GZIPInputStream.GZIP_MAGIC.toByte) && (contents(1) == (GZIPInputStream.GZIP_MAGIC >> 8).toByte))
  }
}