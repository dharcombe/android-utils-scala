package com.gramercysoftware.utils

import android.os.{ Bundle ⇒ AndroidBundle, Message ⇒ AndroidMessage }
import android.os.Handler

case object Message {
  def apply(what: Int, data: Option[AndroidBundle] = None) = {
    val msg = new AndroidMessage()
    msg.what = what
    data match {
      case Some(bundle) ⇒ msg.setData(bundle)
      case None ⇒
    }
    msg
  }
}
