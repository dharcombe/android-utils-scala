package com.gramercysoftware.utils

private[utils] object utils {
  def optionallyGet[T](code: ⇒ T): Option[T] =
    try {
      Some(code)
    } catch {
      case _: Throwable ⇒ None
    }
}