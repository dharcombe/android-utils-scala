package com.gramercysoftware.utils

import scala.collection.immutable.SortedMap
import scala.collection.immutable.TreeMap

trait Sectionable {
  def sectionKey: String
}

object ListSectioner {
  def apply[T <: Sectionable] = new ListSectioner[T]()
}

class ListSectioner[T <: Sectionable] {
  def apply(list: Seq[T], sectionOrder: Option[Ordering[String]] = None, itemOrder: Option[Ordering[T]] = None) = {
    val sections = (list map { i ⇒ i.sectionKey }).distinct

    val sectionedList = (for (section ← sections) yield (section, {
      itemOrder match {
        case Some(order) ⇒ list.filter(i ⇒ i.sectionKey == section).sorted(order)
        case None ⇒ list.filter(i ⇒ i.sectionKey == section)
      }
    }))

    sectionOrder match {
      case Some(order) ⇒ TreeMap.empty(order) ++ sectionedList
      case None ⇒ sectionedList toMap
    }
  }

  def section(list: Seq[T], sectionOrder: Option[Ordering[String]] = None, itemOrder: Option[Ordering[T]] = None) = {
    val sectioned = list groupBy (_.sectionKey)
  }
}