package com.gramercysoftware.utils

import java.net.Authenticator
import java.net.PasswordAuthentication
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager
import javax.net.ssl.HostnameVerifier
import javax.net.ssl.SSLSession
import android.util.Log

object HttpUtils {
  def authenticator(user: String, password: String) = Authenticator.setDefault(new Authenticator() {
    override def getPasswordAuthentication() = new PasswordAuthentication(user, password.toCharArray())
  })

  def nonValidatingTrustManager = new TrustManager {
    new X509TrustManager() {
      override def getAcceptedIssuers(): Array[java.security.cert.X509Certificate] = null
      override def checkClientTrusted(certs: Array[java.security.cert.X509Certificate], authType: String) = {}
      override def checkServerTrusted(certs: Array[java.security.cert.X509Certificate], authType: String) = {}
    }
  }

  def nullHostNameVerifier = new HostnameVerifier {
    def verify(hostname: String, session: SSLSession): Boolean = true
  }
}