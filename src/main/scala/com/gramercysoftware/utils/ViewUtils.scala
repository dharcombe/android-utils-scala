package com.gramercysoftware.utils

import android.app.Activity
import android.view.View
import android.util.Log

object ViewUtils {
  def getViewObject[T <: View](activity: Activity, id: Int): T = {
    activity.findViewById(id) match {
      case null ⇒
        Log.e("ViewUtils", s"Can't find object of id ${id}")
        throw new Exception(s"Can't find object of id ${id}")
      case foo: T ⇒ foo
      case x ⇒
        Log.e("ViewUtils", s"${x.getClass} is not of required type")
        throw new ClassCastException
    }
  }

  def getViewObject[T <: View](activity: Activity, foo: Class[T], id: Int): T = {
    getViewObject(activity, id)
  }

  def getViewObject[T <: View](view: View, id: Int): T = {
    view.findViewById(id) match {
      case null ⇒
        Log.e("ViewUtils", s"Can't find object of id ${id}")
        throw new Exception(s"Can't find object of id ${id}")
      case foo: T ⇒ foo
      case x ⇒
        Log.e("ViewUtils", s"${x.getClass} is not of required type")
        throw new ClassCastException
    }
  }
}