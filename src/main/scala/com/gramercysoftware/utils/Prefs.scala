package com.gramercysoftware.utils

import scala.collection.JavaConversions._
import android.content.{ SharedPreferences ⇒ AndroidSP }
import android.content.SharedPreferences._
import android.content.Context
import android.preference.PreferenceManager

object SharedPreferences {
  def apply(context: Context) =
    new SharedPreferences(PreferenceManager.getDefaultSharedPreferences(context))
}

class SharedPreferences(sharedPreferences: AndroidSP) {
  def getAll: Map[String, _] = sharedPreferences.getAll.toMap

  def getString(key: String, defValue: String): String = sharedPreferences.getString(key, defValue)
  def getStringOption(key: String) = getString(key, null) match {
    case null ⇒ None
    case s ⇒ Some(s)
  }

  def getInt(key: String, defValue: Int): Int = sharedPreferences.getInt(key, defValue)
  def getIntOption(key: String) = sharedPreferences.getInt(key, Int.MinValue) match {
    case Int.MinValue if (getInt(key, Int.MaxValue) != Int.MinValue) ⇒ None
    case i ⇒ Some(i)
  }

  def getLong(key: String, defValue: Long): Long = sharedPreferences.getLong(key, defValue)
  def getLongOption(key: String) = getLong(key, Long.MinValue) match {
    case Long.MinValue if (getLong(key, Long.MaxValue) != Long.MinValue) ⇒ None
    case i ⇒ Some(i)
  }

  def getFloat(key: String, defValue: Float): Float = sharedPreferences.getFloat(key, defValue)
  def getFloatOption(key: String) = getFloat(key, Float.MinValue) match {
    case Float.MinValue if (getFloat(key, Float.MaxValue) != Float.MinValue) ⇒ None
    case i ⇒ Some(i)
  }

  def getBoolean(key: String, defValue: Boolean): Boolean = sharedPreferences.getBoolean(key, defValue)

  def contains(key: String): Boolean = sharedPreferences.contains(key)

  def edit: Editor = sharedPreferences.edit

  def put[T](key: String, value: T)(implicit m: Manifest[T]) = {
    val editor = m.runtimeClass.getSimpleName match {
      case "String" ⇒ edit.putString(key, value.asInstanceOf[String]).commit
      case "int" ⇒ edit.putInt(key, value.asInstanceOf[Int]).commit
      case "double" ⇒ edit.putFloat(key, value.asInstanceOf[Float]).commit
      case "float" ⇒ edit.putFloat(key, value.asInstanceOf[Float]).commit
      case "long" ⇒ edit.putLong(key, value.asInstanceOf[Long]).commit
      case "boolean" ⇒ edit.putBoolean(key, value.asInstanceOf[Boolean]).commit
      case _ ⇒
    }
    this
  }

  def registerOnSharedPreferenceChangeListener(listener: OnSharedPreferenceChangeListener): Unit = sharedPreferences.registerOnSharedPreferenceChangeListener(listener)

  def unregisterOnSharedPreferenceChangeListener(listener: OnSharedPreferenceChangeListener): Unit = sharedPreferences.unregisterOnSharedPreferenceChangeListener(listener)
}