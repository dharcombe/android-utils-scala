package com.gramercysoftware.db

import android.database.sqlite.SQLiteDatabase

trait DbHelper {
  def forceRecreateDatabase(db: SQLiteDatabase)
}