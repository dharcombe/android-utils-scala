package com.gramercysoftware.db

import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import android.database.Cursor
import android.content.ContentValues
import android.database.sqlite.SQLiteStatement

class DbAdapterException extends Exception

trait DbAdapter[T <: Storable] {
  def DATABASE_VERSION = 1
  final def K_NAME = "id"
  final def K_COL = 0

  def DB_NAME: String
  def DB_TABLE: String

  def dbHelper: SQLiteOpenHelper
  private var db: Option[SQLiteDatabase] = None

  def createValues(item: T): ContentValues
  def createObject(cursor: Cursor): T
  def dbCreateStatement(): String
  def dbBulkInsertStatement: Option[String] = None
  def dbBulkInsertBinder(statement: SQLiteStatement, item: T): Boolean = false

  def database = db match {
    case Some(database) ⇒ database
    case None ⇒ open
  }

  def open = {
    val database = dbHelper.getWritableDatabase
    db = Some(database)
    database
  }

  def close = database.close

  private[this] def execute[R](f: ⇒ R): R = {
    database.isOpen match {
      case true ⇒ f
      case false ⇒
        open
        val result = f
        close
        result
    }
  }

  private[this] def fetchCursor(cursor: Cursor, list: Seq[T] = Seq()): Seq[T] = {
    cursor.isAfterLast match {
      case false ⇒
        val o = createObject(cursor)
        cursor.moveToNext
        fetchCursor(cursor, list :+ o)
      case true ⇒ list
    }
  }

  def getAll(order: String) = {
    val cursor = execute(database.query(DB_TABLE, null, null, null, null, null, null, order))
    val seq = cursor.moveToFirst match {
      case true ⇒ fetchCursor(cursor)
      case false ⇒ Seq()
    }
    cursor.close
    seq
  }

  def delete(id: Long) = execute(database.delete(DB_TABLE, (K_NAME + "=?"), Array[String](id.toString)) > 0)

  def deleteAll() = execute(database.delete(DB_TABLE, null, null))

  def fetchById(id: Long): Option[T] = fetch((K_NAME, id))

  def fetch(where: (String, _)*): Option[T] = {
    val (columns, values) = where.foldLeft((Seq[String](), Seq[String]())) { (s, p) ⇒ (s._1 :+ s"${p._1} = ?", s._2 :+ p._2.toString) }

    val cursor = execute(database.query(DB_TABLE, null, columns.mkString(", "), values.toArray, null, null, null))
    cursor.moveToFirst match {
      case true ⇒ Some(createObject(cursor))
      case false ⇒ None
    }
  }

  def insert(item: T): Long = {
    val values = createValues(item)
    execute(database.insert(DB_TABLE, null, values))
  }

  def insert(items: Seq[T]): Long = {
    open
    dbBulkInsertStatement match {
      case Some(s) ⇒
        val statement = database.compileStatement(s)
        database.beginTransaction
        items foreach { item ⇒
          statement.clearBindings
          dbBulkInsertBinder(statement, item)
          statement.execute
        }
        database.setTransactionSuccessful
        database.endTransaction
      case None ⇒ items foreach (item ⇒ database.insert(DB_TABLE, null, createValues(item)))
    }
    close
    items.length
  }

  def update(item: T) = {
    val values = createValues(item)
    execute(database.update(DB_TABLE, values, K_NAME + "=" + item.id, null))
  }

}
