package com.gramercysoftware.db

trait Storable {
  def id: Long
}