package com.gramercysoftware.utils;

import android.os.AsyncTask;

/**
 * Temporary workaround to solve a Scala compiler issue which shows up
 * at runtime with the error message
 * "java.lang.AbstractMethodError: abstract method not implemented"
 * for the missing method LookupTask.doInBackground(String... args).
 * 
 * Our solution: the Java method doInBackground(String... args) forwards
 * the call to the Scala method doInBackground1(String[] args).
 */
public abstract class AsyncTaskWrapper<T> extends AsyncTask<String, Void, T> {

	protected abstract T doInBackground1(String[] args);

	@Override
	protected T doInBackground(String... args) {
		String[] args1 = new String[args.length];
		for (int i = 0; i < args.length; i++) {
			args1[i] = args[i];
		}
		return doInBackground1(args1);
	}

}