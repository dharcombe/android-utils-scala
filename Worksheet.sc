import com.gramercysoftware.utils.Compressor

object Worksheet {
  val source = "This is a test stream"            //> source  : String = This is a test stream
  
  val c = Compressor.gzip(source.getBytes)        //> c  : Array[Byte] = Array(31, -117, 8, 0, 0, 0, 0, 0, 0, 0, 11, -55, -56, 44,
                                                  //|  86, 0, -94, 68, -123, -110, -44, -30, 18, -123, -30, -110, -94, -44, -60, 9
                                                  //| 2, 0, -40, -1, 25, 5, 21, 0, 0, 0)
  
  new String(Compressor.unGzip(c))                //> res0: String = This is a test stream
}